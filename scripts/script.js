const sup = document.querySelector(".sup");
const operador = document.querySelector(".operador");
const inf = document.querySelector(".inf");
const botonBorrar = document.querySelector(".boton__borrar");
const botonIgual = document.querySelector(".boton__igual");
const botonC = document.querySelector(".boton__C");
const botonSigno = document.querySelector("#boton__signo");

const numeros = document.querySelectorAll(".numeros .numero");
const operadoresIzq = document.querySelectorAll(".operadores-izq input");
const operadoresDer = document.querySelectorAll(".operadores-der input");

console.log(numeros.length);
console.log(operadoresIzq.length);
console.log(operadoresDer.length);

for (let j = 0; j < operadoresIzq.length; j++) {
  operadoresIzq[j].addEventListener("click", function () {
    console.log(Number(sup.textContent));
    if (Number.isInteger(Number(sup.value))) {
      operador.textContent = operadoresIzq[j].value;
    }
  });
}

botonSigno.addEventListener("click", function () {
  if (inf.textContent !== "") {
    inf.textContent = Number(inf.textContent) * -1;
    inf.value = inf.textContent;
    console.log(inf.textContent);
  } else {
    sup.textContent = Number(sup.textContent) * -1;
    sup.value = sup.textContent;
    if (sup.textContent === "") {
      sup.textContent = 0;
      sup.value = sup.textContent;
    }
    operador.textContent = "";
  }
});

botonC.addEventListener("click", function () {
  sup.textContent = 0;
  sup.value = sup.textContent;
  operador.textContent = "";
  inf.textContent = "";
});

botonBorrar.addEventListener("click", function () {
  if (inf.textContent !== "") {
    inf.textContent = inf.textContent.slice(0, -1);
  } else {
    sup.textContent = sup.textContent.slice(0, -1);
    sup.value = sup.textContent;
    if (sup.textContent === "") {
      sup.textContent = 0;
      sup.value = sup.textContent;
    }
    operador.textContent = "";
  }
});

botonIgual.addEventListener("click", function () {
  console.log(operador.textContent);
  switch (operador.textContent) {
    case "+":
      suma(sup.value, inf.value);
      operador.textContent = "";
      inf.textContent = "";
      inf.value = "";

      break;
    case "-":
      resta(sup.value, inf.value);
      operador.textContent = "";
      inf.textContent = "";
      break;
    case "X":
      multi(sup.value, inf.value);
      operador.textContent = "";
      inf.textContent = "";
      break;
    case "/":
      divi(sup.value, inf.value);
      operador.textContent = "";
      inf.textContent = "";

      break;
    default:
      break;
  }
});

for (let index = 0; index < numeros.length; index++) {
  numeros[index].addEventListener("click", function () {
    if (
      operador.textContent === "/" ||
      operador.textContent === "X" ||
      operador.textContent === "+" ||
      operador.textContent === "-"
    ) {
      inf.textContent += numeros[index].value;
      //   inf.textContent  cambiar por la wea de abajo
      inf.value = inf.textContent;
    } else {
      if (Number(sup.textContent) === 0) {
        sup.textContent = "";
      }
      sup.textContent += numeros[index].value;
      sup.value = sup.textContent;
    }
  });
}

function suma(n1, n2) {
  sup.textContent = parseInt(n1) + parseInt(n2);
  sup.value = sup.textContent;
  sup.value = sup.textContent;
}

function resta(n1, n2) {
  sup.textContent = parseInt(n1) - parseInt(n2);
  sup.value = sup.textContent;
}

function multi(n1, n2) {
  sup.textContent = parseInt(n1) * parseInt(n2);
  sup.value = sup.textContent;
}

function divi(n1, n2) {
  sup.textContent = parseInt(n1) / parseInt(n2);
  sup.value = sup.textContent;
}
